using System;
using AutoMapper;
using Game.Domain;
using JetBrains.Annotations;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IUserRepository userRepository;
        public UsersController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpGet("{userId:Guid}", Name = nameof(GetUserById))]
        public ActionResult<UserDto> GetUserById([FromRoute] Guid userId)
        {
            var user = userRepository.FindById(userId);
            if (user == null) return NotFound();
            var userDto = Mapper.Map<UserDto>(user);

            return Ok(userDto);
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] UserToCreateDto user)
        {
            if (user == null) return BadRequest(); 
            if (!ModelState.IsValid) return UnprocessableEntity(ModelState);

            var createdUserEntity = userRepository.Insert(Mapper.Map<UserEntity>(user));
            return CreatedAtRoute(
                nameof(GetUserById),
                new { userId = createdUserEntity.Id },
                createdUserEntity.Id);
        }

        [HttpPut("{userId}")]
        public IActionResult UpdateUser([FromBody] UserToUpdateDto user, [FromRoute] Guid userId)
        {
            if (user == null || userId == Guid.Empty)
                return BadRequest();

            if (!ModelState.IsValid) return UnprocessableEntity(ModelState);

            var newUserEntity = new UserEntity(userId);
            var userEntity = Mapper.Map(user, newUserEntity);
            userRepository.UpdateOrInsert(userEntity, out var isInserted);
           
            if (isInserted)
            {
                return CreatedAtRoute(
                    nameof(GetUserById),
                    new { userId = userEntity.Id },
                    userEntity.Id);
            }

            return NoContent();
        }

        [HttpPatch("{userId}")]
        public IActionResult PartiallyUpdateUser([FromBody] JsonPatchDocument<UpdateDto> patchDoc, [FromRoute] Guid userId)
        {
            if (patchDoc == null || userId == Guid.Empty)
                return BadRequest();

            if (!ModelState.IsValid) return UnprocessableEntity(ModelState);
            patchDoc.ApplyTo(updateDto, ModelState);
        }
    }
}