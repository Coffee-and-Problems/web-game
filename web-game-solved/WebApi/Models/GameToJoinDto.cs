using System;

namespace WebApi.Models
{
    public class GameToJoinDto
    {
        public Guid PlayerId { get; set; }
    }
}